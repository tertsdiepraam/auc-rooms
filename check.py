import numpy as np
import pandas as pd

df = pd.read_csv('rooms.csv')

count = pd.Series(
    data = np.zeros(((2080-630)//2,)),
    index = np.arange(630, 2080, 2)
)

for row in df.iterrows():
    content = row[1]
    try:
        count.loc[int(content['Low']):int(content['High'])] += 1
    except ValueError:
        pass

pd.options.display.max_rows = None
print(count)
