let data;
let svgTag = document.getElementById("svgelement");
let share_button = document.getElementById("share-button");
let sections;
let floornumbers;
let entrances;
let src_param;
let url_prefix;

roomnumber_input = document.getElementById("roomnumber");
response_p = document.getElementById("response");

roomnumber_input.addEventListener("change", () => {
    window.history.pushState({}, document.title, location.protocol + "//" + location.host + location.pathname + "?room=" + roomnumber.value)
    update_room_info();
});

window.onpopstate = () => {
    const params = new URLSearchParams(document.location.search);
    roomnumber_input.value = params.get("room");
    update_room_info();
};

function update_room_info() {
    if (roomnumber.value === "" || typeof sections === 'undefined') {
        return;
    }

    let room = findRoom(roomnumber.value);
    response_p.innerHTML = formatDescription(room);

    if (room !== undefined || typeof room === "string") {
        let section_str = room.Section.toString();
        for (let entrance of entrances) {
            entrance.style.fill = "#00000000";
        }
        entrances[getEntrance(parseInt(roomnumber.value))-1].style.fill = '#FF0000';
        for (let section of sections) {
            if (section.id.substring(7) === section_str) {
                section.style.fill = "#FF0000";
            } else {
                section.style.fill = "#00000000";
            }
        }

        for (let floornumber of floornumbers) {
            if (floornumber.id.substring(11) === section_str) {
                floornumber.innerHTML = room.Floor.toString();
            } else {
                floornumber.innerHTML = "";
            }
        }
    }

}


Papa.parse("rooms.csv", {
    header: true,
    dynamicTyping: true,
    download: true,
    complete: (results) => {
        data = results.data;
        if (typeof entrances !== 'undefined') {
            update_room_info();
        }
    }
});

window.addEventListener("load", () => {
    const params = new URLSearchParams(document.location.search);
    roomnumber_input.value = params.get("room");2
    
    src_param = params.get("parent");
    console.log(src_param);
    url_prefix = src_param || location.protocol + "//" + location.host + location.pathname;
    console.log(url_prefix);
    let svgdoc = svgTag.contentDocument;

    let sectionNodeList = svgdoc.getElementsByClassName("section");
    sections = Array.prototype.slice.call(sectionNodeList);
    
    // All ids must be of the form section0, section1, etc.
    // We sort based on that index.
    sections.sort((a,b) => a.id.substring(7)-b.id.substring(7));

    for (let section of sections) {
        section.style.fill = "#00000000";
    }

    let floornumberNodeList = svgdoc.getElementsByClassName("floornumber");
    floornumbers = Array.prototype.slice.call(floornumberNodeList);
    
    floornumbers.sort((a,b) => a.id.substring(11)-b.id.substring(11));

    for (let floornumber of floornumbers) {
        floornumber.innerHTML = "";
    }

    let entranceNodeList = svgdoc.getElementsByClassName("entrance");
    entrances = Array.prototype.slice.call(entranceNodeList);

    entrances.sort((a,b) => a.id.substring(8)-b.id.substring(8));

    for (let entrance of entrances) {
        entrance.style.fill = "#00000000";
    }

    if (typeof data !== 'undefined') {
        update_room_info();
    }
})

roomnumber_input.addEventListener("copy", event => {
    event.preventDefault();
    if (event.clipboardData) {
        event.clipboardData.setData("text/plain", url_prefix + "?room=" + roomnumber_input.value)
    }
})

function share() {
    roomnumber_input.setSelectionRange(0, roomnumber_input.value.length);
    roomnumber_input.focus()
    document.execCommand("copy");
    roomnumber_input.blur()
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copied link!";
}

function outFunc() {
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copy to clipboard";
}

function checkRoom(n) {
    return n >= 630 && n <= 2780 && n % 2 === 0
}

function findRoom(n) {
    n = parseFloat(n);
    if (n !== parseInt(n, 10)) {
        return "decimal"
    } else if (n < 630) {
        return "too low"
    } else if (n > 2780) {
        return "too high"
    } else if (n % 2 !== 0) {
        return "odd"
    }

    for (const row of data) {
        if (row.Low <= n && n <= row.High) {
            return row;
        }
    }
}

function formatDescription(room) {
    if (room === undefined) {
        share_button.disabled = true;
        return "That's not a valid room number...";
    }

    if (room === "decimal") {
        return "A decimal number??? Really?!"
    }

    if (room === "too low") {
        return "That number is too low..."
    }

    if (room === "too high") {
        return "That number is too high..."
    }

    if (room === "odd") {
        return "That number is on the other side of the street..."
    }

    building = {
        1: 'closest',
        2: 'middle',
        3: 'furthest',
    }[room.Building];

    floor = {
        0: 'ground',
        1: 'first',
        2: 'second',
        3: 'third',
        4: 'fourth',
        5: 'fifth',
    }[room.Floor];

    share_button.disabled = false;
    return `It's in the ${building} building on the ${floor} floor.`
}

function getEntrance(room) {
    if (room >= 630 && room <= 778) {
        return 12;
    } else if (room >= 780 && room <= 784) {
        return 11;
    } else if (room >= 786 && room <= 794) {
        return 10;
    } else if (room >= 796 && room <= 1172) {
        return 9;
    } else if (room >= 1174 && room <= 1184) {
        return 8
    } else if (room >= 1186 && room <= 1414) {
        return 7;
    } else if (room >= 1416 && room <= 1422) {
        return 6;
    } else if (room >= 1424 && room <= 1668) {
        return 5;
    } else if (room >= 1670 && room <= 1676) {
        return 4
    } else if (room >= 1674 && room <= 1924) {
        return 3;
    } else if (room >= 1926 && room <= 1928) {
        return 2;
    } else if (room >= 1930 && room <= 2078) {
        return 1;
    }
}
