# AUC Room Finder
Are you a student at Amsterdam University College? This website will help you
find your friends' rooms! It is automatically hosted on on
[tertsdiepraam.gitlab.com/auc-rooms](tertsdiepraam.gitlab.com/auc-rooms).

Feel free to fork and make your own version or make a PR for this repository.

## Local Usage
After cloning the repository, you can use any simple http server to host a local
version.